# Sample Apache Sling Project

This is a project template for Apache Sling projects. 
This Project was setup to work with Sling 11.

## Sample APIs
This exposes three apis:

* http://localhost:8080/users/all
* http://localhost:8080/users/byUsername?username=someUserName
* http://localhost:8080/users/byFirstLetterLastName?lastNamePrefix='S'


See [Sling Getting Started](https://sling.apache.org/documentation/getting-started.html) for more info.
