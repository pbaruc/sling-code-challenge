package com.pbaruc.sample.servlets;

import org.apache.sling.testing.mock.sling.junit.SlingContext;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;

/**
 * Unit tests for {@link GetByUsernameServlet}.
 */
public class GetByUsernameServletTest {
    private final static String TEST_USER_NAME = "blueelephant921";

    @Rule
    public final SlingContext context = new SlingContext();

    // Servlet To Test
    private GetByUsernameServlet servlet = new GetByUsernameServlet();

    @Test
    public void testDoGet() throws IOException {
        // mock request/response
        MockSlingHttpServletRequest request = new MockSlingHttpServletRequest(this.context.bundleContext());
        MockSlingHttpServletResponse response = new MockSlingHttpServletResponse();

        // add username query params
        request.setQueryString("username=" + TEST_USER_NAME);

        // execute GET on the servlet
        servlet.doGet(request, response);

        // extract response
        String responseStr = response.getOutputAsString();
        Assert.assertNotNull(responseStr);

        // json parse the response
        JSONObject responseJSON = new JSONObject(responseStr);

        // status should be ok
        String status = responseJSON.getString("status");
        Assert.assertEquals("OK", status);

        // we should have user
        JSONObject user = responseJSON.getJSONObject("user");
        Assert.assertNotNull(user);

        // user should have username that matches TEST_USER_NAME
        JSONObject login = user.getJSONObject("login");
        String username = login.getString("username");
        Assert.assertEquals(TEST_USER_NAME, username);

    }

    @Test
    public void testInvalidParamsGet() throws IOException {
        // mock request/response
        MockSlingHttpServletRequest request = new MockSlingHttpServletRequest(this.context.bundleContext());
        MockSlingHttpServletResponse response = new MockSlingHttpServletResponse();

        // add username query params
        request.setQueryString("badParam=" + TEST_USER_NAME);

        // execute GET on the servlet
        servlet.doGet(request, response);

        // extract response
        String responseStr = response.getOutputAsString();
        Assert.assertNotNull(responseStr);

        // json parse the response
        JSONObject responseJSON = new JSONObject(responseStr);

        // status should be ok
        String status = responseJSON.getString("status");
        Assert.assertEquals("FAILED", status);

    }


    @Test
    public void testInvalidUserName() throws IOException {
        // mock request/response
        MockSlingHttpServletRequest request = new MockSlingHttpServletRequest(this.context.bundleContext());
        MockSlingHttpServletResponse response = new MockSlingHttpServletResponse();

        // add username query params
        request.setQueryString("username=" + "XXX001");

        // execute GET on the servlet
        servlet.doGet(request, response);

        // extract response
        String responseStr = response.getOutputAsString();
        Assert.assertNotNull(responseStr);

        // json parse the response
        JSONObject responseJSON = new JSONObject(responseStr);

        // status
        String status = responseJSON.getString("status");
        Assert.assertNotNull(status);
        Assert.assertEquals("FAILED", status);

        // error-message
        String errorMessage = responseJSON.getString("error-message");
        Assert.assertNotNull(errorMessage);
        Assert.assertEquals("invalid username", errorMessage);







    }


}
