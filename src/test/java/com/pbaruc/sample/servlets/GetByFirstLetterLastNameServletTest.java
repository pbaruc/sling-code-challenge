package com.pbaruc.sample.servlets;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.junit.SlingContext;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;

/**
 */
public class GetByFirstLetterLastNameServletTest {
    private final static String LAST_NAME_PREFIX = "s";

    @Rule
    public final SlingContext context = new SlingContext();

    // Servlet to Test
    private GetByFirstLetterLastNameServlet servlet = new GetByFirstLetterLastNameServlet();

    @Test
    public void testDoGet() throws IOException {
        // mock request/response
        MockSlingHttpServletRequest request = new MockSlingHttpServletRequest(this.context.bundleContext());
        MockSlingHttpServletResponse response = new MockSlingHttpServletResponse();

        // add letter to query params
        request.setQueryString("lastNamePrefix=" + LAST_NAME_PREFIX);

        // execute GET on the servlet
        servlet.doGet(request, response);

        // extract response
        String responseStr = response.getOutputAsString();
        Assert.assertNotNull(responseStr);

        // json parse the response
        JSONObject responseJSON = new JSONObject(responseStr);

        // status should be ok
        String status = responseJSON.getString("status");
        Assert.assertEquals("OK", status);

        // we should have multiple users
        JSONArray users = responseJSON.getJSONArray("users");
        Assert.assertNotNull(users);
        Assert.assertTrue(users.length() > 0);

        // each user's lastname should start with letter
        users.forEach(user -> {
            JSONObject userObject = (JSONObject) user;
            String lastName = userObject.getString("lastName");

            Assert.assertNotNull(lastName);
            Assert.assertTrue(lastName.toLowerCase().startsWith(LAST_NAME_PREFIX.toLowerCase()));
        });

    }

    @Test
    public void testInvalidParamsGet() throws IOException {
        // mock request/response
        MockSlingHttpServletRequest request = new MockSlingHttpServletRequest(this.context.bundleContext());
        MockSlingHttpServletResponse response = new MockSlingHttpServletResponse();

        // add letter to query params
        request.setQueryString("badQueryParam=" + LAST_NAME_PREFIX);

        // execute GET on the servlet
        servlet.doGet(request, response);

        // extract response
        String responseStr = response.getOutputAsString();
        Assert.assertNotNull(responseStr);

        // json parse the response
        JSONObject responseJSON = new JSONObject(responseStr);

        // status should be ok
        String status = responseJSON.getString("status");
        Assert.assertEquals("FAILED", status);
    }


    @Test
    public void testNoMatch() throws IOException {
        // mock request/response
        MockSlingHttpServletRequest request = new MockSlingHttpServletRequest(this.context.bundleContext());
        MockSlingHttpServletResponse response = new MockSlingHttpServletResponse();

        // add letter to query params
        request.setQueryString("lastNamePrefix=X");

        // execute GET on the servlet
        servlet.doGet(request, response);

        // extract response
        String responseStr = response.getOutputAsString();
        Assert.assertNotNull(responseStr);

        // json parse the response
        JSONObject responseJSON = new JSONObject(responseStr);

        // status should be ok
        String status = responseJSON.getString("status");
        Assert.assertEquals("OK", status);

        // we should have multiple users
        JSONArray users = responseJSON.getJSONArray("users");
        Assert.assertNotNull(users);
        Assert.assertTrue(users.length() == 0);

        // each user's lastname should start with letter
        users.forEach(user -> {
            JSONObject userObject = (JSONObject) user;
            String lastName = userObject.getString("lastName");

            Assert.assertNotNull(lastName);
            Assert.assertTrue(lastName.toLowerCase().startsWith(LAST_NAME_PREFIX.toLowerCase()));
        });

    }

}
