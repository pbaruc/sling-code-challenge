package com.pbaruc.sample.servlets;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.junit.SlingContext;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;

/**
 * Unit tests for {@link GetAllServlet}.
 */
public class GetAllServletTest {

    @Rule
    public final SlingContext context = new SlingContext();

    // Servlet To Test
    private GetAllServlet servlet = new GetAllServlet();

    @Test
    public void testDoGet() throws IOException {
        // mock request/response
        SlingHttpServletRequest request = new MockSlingHttpServletRequest(this.context.bundleContext());
        MockSlingHttpServletResponse response = new MockSlingHttpServletResponse();

        // execute GET on the servlet
        servlet.doGet(request, response);

        // extract response
        String responseStr = response.getOutputAsString();

        // json parse the response
        JSONObject responseJSON = new JSONObject(responseStr);

        // status should be ok
        String status = responseJSON.getString("status");
        Assert.assertEquals("OK", status);

        // we should have users
        JSONArray users = responseJSON.getJSONArray("users");
        Assert.assertNotNull(users);

        // we should have 100 users
        Assert.assertEquals(100, users.length());


    }
}
