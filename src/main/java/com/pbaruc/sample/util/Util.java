package com.pbaruc.sample.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Util {


    /**
     * Fetch content at the url enpoint.
     *
     * @param theUrl
     * @return Url Content
     * @throws IOException
     */
    public static String fetchURLContent(String theUrl) throws IOException {
        StringBuilder content = new StringBuilder();

        // create a url object
        URL url = new URL(theUrl);

        // create a urlconnection object
        URLConnection urlConnection = url.openConnection();

        // wrap the urlconnection in a bufferedreader
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

        String line;

        // read from the urlconnection via the bufferedreader
        while ((line = bufferedReader.readLine()) != null) {
            content.append(line + "\n");
        }
        bufferedReader.close();

        return content.toString();
    }


}
