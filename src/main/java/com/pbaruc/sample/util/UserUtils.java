package com.pbaruc.sample.util;

import com.pbaruc.sample.models.UserModel;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserUtils {


    /**
     * Converts the content string into UserModel objects.
     *
     * @param content
     * @return
     * @throws IOException
     */
    public static List<UserModel> convertToUsers(String content) throws IOException {
        JSONObject jsonObject = new JSONObject(content);

        List<UserModel> retVal = new ArrayList();
        JSONArray results = jsonObject.getJSONArray("results");
        for (int i = 0; i < results.length(); i++) {
            UserModel userModel = new UserModel(results.getJSONObject(i));
            retVal.add(userModel);
        }
        return retVal;
    }


}
