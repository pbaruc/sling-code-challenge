package com.pbaruc.sample.models;

import org.json.JSONObject;

public class UserModel {
    private JSONObject source;

    public UserModel(JSONObject source) {
      this.source = source;
    }

    public JSONObject getSource() {
      return this.source;
    }

    public String getLastName() {
        JSONObject name = source.getJSONObject("name");
        return name.getString("last");
    }

    public String getUserName() {
        try {
            JSONObject login = source.getJSONObject("login");
            return login.getString("username");
        } catch(Exception e) {
            return "";
        }
    }
}
