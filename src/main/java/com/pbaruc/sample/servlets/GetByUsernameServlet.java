package com.pbaruc.sample.servlets;

import com.pbaruc.sample.models.UserModel;
import com.pbaruc.sample.util.UserUtils;
import com.pbaruc.sample.util.Util;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_METHODS;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_PATHS;


/**
 * This servlet returns user based on a specified username
 */
@Component(
        service = Servlet.class,
        property = {
                SLING_SERVLET_PATHS + "=/users/byUsername",
                SLING_SERVLET_METHODS + "=GET"
        }
)
public class GetByUsernameServlet extends SlingSafeMethodsServlet {
    private static final Logger logger = LoggerFactory.getLogger(GetByUsernameServlet.class);
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String USERS_URL = "https://randomuser.me/api/?results=100&seed=foobar";

    @Override
    protected void doGet(SlingHttpServletRequest request,
                         SlingHttpServletResponse response) throws IOException {

        JSONObject apiResponse = new JSONObject();
        try {

            // resolve the user name
            String username = resolveUserName(request);
            logger.info("Username: " + username);

            UserModel user = existsInCache(username);

            // In the cache?
            if(user != null) {

                // form ok response
                apiResponse.put("user", user.getSource());
                apiResponse.put("status", "OK");

            }

            // Fetch fresh...
            else {

                // confirm there is a user
                user = fetchAndCache(username);
                logger.info("User: " + user);

                // form ok response
                apiResponse.put("user", user.getSource());
                apiResponse.put("status", "OK");
            }
        }

        // form failed response
        catch (Exception e) {
            apiResponse.put("status", "FAILED");
            apiResponse.put("error-message", e.getMessage());
        }

        response.setStatus(SC_OK);
        response.setContentType(CONTENT_TYPE_JSON);
        response.getWriter().write(apiResponse.toString());
    }

    /**
     * Caching implementation..
     *
     * If we have time do Cache implementation.
     *
     * @param username
     * @return UserModel
     */
    private UserModel existsInCache(String username) {
        // we query the cache to see if there is a valid value for this key
        // TODO: Add cache implementation

        // if not then we just return null
        return null;
    }

    private UserModel fetchAndCache(String username) throws IOException {
        // fetch the users
        List<UserModel> users = fetchUsers();
        logger.info("Users: " + users);

        // filter the users
        List<UserModel> filteredUsers = filter(users, username);
        logger.info("FilteredUsers: " + filteredUsers);

        // confirm there is a user
        UserModel user = confirmUser(filteredUsers);

        // cache the result
        // TODO: Add cache implementation
        return user;
    }

    /**
     * Ensure we have a valid username.
     *
     * @param request
     * @return Username
     */
    private String resolveUserName(SlingHttpServletRequest request) {
        String userName = request.getParameter("username");
        if(userName == null) {
            throw new IllegalArgumentException();
        }

        if(userName.trim().length() == 0) {
            throw new IllegalArgumentException();
        }

        return userName;
    }

    /**
     * Fetch all the users from the USERS_URL
     *
     * @return List of users
     * @throws IOException
     */
    private List<UserModel> fetchUsers() throws IOException {
        String usersUrlContent = Util.fetchURLContent(USERS_URL);
        List<UserModel> retVal = UserUtils.convertToUsers(usersUrlContent);
        return retVal;
    }

    /**
     * Filter for users who's username matches the given username.
     *
     * @param allUsers
     * @param username
     * @return List Of Users
     */
    private List<UserModel> filter(List<UserModel> allUsers, String username) {
        List<UserModel> filtered = allUsers.stream().filter(user -> {

            // does user's username match the specified username?
            if(user.getUserName().equals(username)) {
                return true;
            }

            return false;
        }).collect(Collectors.toList());
        return filtered;
    }

    /**
     * Ensure there is at least one user in the users list, then return
     * the first user.
     *
     * @param users
     * @return User
     */
    private UserModel confirmUser(List<UserModel> users) {

        // no users means invalid username
        if(users == null || users.isEmpty()) {
            throw new IllegalArgumentException("invalid username");
        }

        // there really should only be 1 user
        if(users.size() != 1) {
            logger.warn("Found more than one user for requested username");
        }

        return users.get(0);
    }
}
