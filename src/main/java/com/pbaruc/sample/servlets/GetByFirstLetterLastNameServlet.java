package com.pbaruc.sample.servlets;

import com.pbaruc.sample.models.UserModel;
import com.pbaruc.sample.util.UserUtils;
import com.pbaruc.sample.util.Util;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_METHODS;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_PATHS;


/**
 * This servlet will return users based on the first letter of the last name.
 */
@Component(
        service = Servlet.class,
        property = {
                SLING_SERVLET_PATHS + "=/users/byFirstLetterLastName",
                SLING_SERVLET_METHODS + "=GET"
        }
)
public class GetByFirstLetterLastNameServlet extends SlingSafeMethodsServlet {
    private static final Logger logger = LoggerFactory.getLogger(GetByFirstLetterLastNameServlet.class);
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String USERS_URL = "https://randomuser.me/api/?results=100&seed=foobar";

    @Override
    protected void doGet(SlingHttpServletRequest request,
                         SlingHttpServletResponse response) throws IOException {

        JSONObject apiResponse = new JSONObject();
        try {

            // resolve the letter
            String lastNamePrefix = resolveLastNamePrefix(request);
            logger.info("lastNamePrefix: " + lastNamePrefix);

            // fetch the users
            List<UserModel> users = fetchUsers();
            logger.info("Users: " + users);

            // filter the users
            List<UserModel> filteredUsers = filter(users, lastNamePrefix);
            logger.info("FilteredUsers: " + filteredUsers);

            apiResponse.put("users", filteredUsers);
            apiResponse.put("status", "OK");
        } catch (Exception e) {
            apiResponse.put("status", "FAILED");
            apiResponse.put("error-message", e.getMessage());
        }

        response.setStatus(SC_OK);
        response.setContentType(CONTENT_TYPE_JSON);
        response.getWriter().write(apiResponse.toString());
    }


    /**
     * Ensure we have a valid letter.
     *
     * @param request
     * @return Username
     */
    private String resolveLastNamePrefix(SlingHttpServletRequest request) {
        String lastNamePrefix = request.getParameter("lastNamePrefix");
        if(lastNamePrefix == null) {
            throw new IllegalArgumentException();
        }

        if(lastNamePrefix.trim().length() == 0) {
            throw new IllegalArgumentException();
        }

        return lastNamePrefix;
    }


    /**
     * Fetch all the users from the USERS_URL
     *
     * @return List of users
     * @throws IOException
     */
    private List<UserModel> fetchUsers() throws IOException {
        String usersUrlContent = Util.fetchURLContent(USERS_URL);
        List<UserModel> retVal = UserUtils.convertToUsers(usersUrlContent);
        return retVal;
    }

    /**
     * Filter for users who's last name starts with specified letter.
     *
     * @param allUsers
     * @param lastNamePrefix
     * @return List Of Users
     */
    private List<UserModel> filter(List<UserModel> allUsers, String lastNamePrefix) {
        List<UserModel> filtered = allUsers.stream().filter(user -> {


            // does user's last name start with lastNamePrefix?
            if(user.getLastName().toLowerCase().startsWith(lastNamePrefix.toLowerCase())) {
                return true;
            }

            return false;
        }).collect(Collectors.toList());
        return filtered;
    }

}
