package com.pbaruc.sample.servlets;

import com.pbaruc.sample.models.UserModel;
import com.pbaruc.sample.util.UserUtils;
import com.pbaruc.sample.util.Util;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_METHODS;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_PATHS;


/**
 * This servlet returns all users.
 */
@Component(
        service = Servlet.class,
        property = {
                SLING_SERVLET_PATHS + "=/users/all",
                SLING_SERVLET_METHODS + "=GET"
        }
)
public class GetAllServlet extends SlingSafeMethodsServlet {
    private static final Logger logger = LoggerFactory.getLogger(GetAllServlet.class);
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String USERS_URL = "https://randomuser.me/api/?results=100&seed=foobar";

    @Override
    protected void doGet(SlingHttpServletRequest request,
                         SlingHttpServletResponse response) throws IOException {

        JSONObject apiResponse = new JSONObject();
        try {

            // fetch all the users
            List<UserModel> users = fetchUsers();

            // form ok response
            apiResponse.put("users", users);
            apiResponse.put("status", "OK");
        } catch (Exception e) {
            apiResponse.put("status", "FAILED");
            apiResponse.put("error-message", e.getMessage());
        }

        response.setStatus(SC_OK);
        response.setContentType(CONTENT_TYPE_JSON);
        response.getWriter().write(apiResponse.toString());
    }

    /**
     * Fetch all the users from the USERS_URL
     *
     * @return List of users
     * @throws IOException
     */
    private List<UserModel> fetchUsers() throws IOException {
        String usersUrlContent = Util.fetchURLContent(USERS_URL);
        List<UserModel> retVal = UserUtils.convertToUsers(usersUrlContent);
        return retVal;
    }

    private List<UserModel> filter(List<UserModel> allUsers) {
        List<UserModel> filtered = allUsers.stream().filter(user -> user.getLastName().startsWith("S")).collect(Collectors.toList());
        return filtered;
    }

}
